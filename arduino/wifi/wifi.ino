
#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <Stepper.h>
#include <Servo.h>

const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution

/* Set these to your desired credentials. */
const char *ssid = "divebot";
const char *password = "linus";

ESP8266WebServer server(80);
Stepper myStepper(stepsPerRevolution, D5, D6, D7, D8);
Servo lenkservo;
int speed  = 0;

/* Just a little test message.  Go to http://192.168.4.1 in a web browser
 * connected to this access point to see it.
 */
void handleRoot() {
  lenkservo.write(server.arg("lenk").toInt());
  speed = server.arg("speed").toInt();
  if(speed > 0)
    myStepper.setSpeed(speed);
	server.send(200, "text/html", "<h1>You are connected</h1>");
}

void setup() {
	delay(1000);
	Serial.begin(115200);
	Serial.println();

  lenkservo.attach(D4);
  lenkservo.write(90);
 
	Serial.print("Configuring access point...");
	/* You can remove the password parameter if you want the AP to be open. */
	WiFi.softAP(ssid, password);

	IPAddress myIP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(myIP);
	server.on("/", handleRoot);
	server.begin();
	Serial.println("HTTP server started");
}

void loop() {
	server.handleClient();
  if(speed > 0)
  myStepper.step(stepsPerRevolution / 100);
}
