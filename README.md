# Divebot

Das ist die Software für ein kleines Auto, dass man über WLAN mit einem Tablet über die Bewegungssensoren steuern kann.

## Hardware
Als Chip auf dem Auto wird ein Wemos verwendet. Er hat bereits eine WLAN Schnittstelle

### Lenkung
Für die Lenkung wird ein Modellbau-Servo verwendet. 

Anschluss:
* Rot: +5V
* Braun: GND
* Orange: D4

### Antrieb
Als Antrieb werden zwei Schrittmotoren verwendet.
Beide Motoren werden parallel angeschlossen, da sie sich immer gleich verhalten sollen. 

Anschluss:
* + : +5V
* - : GND
* INT 1: D5
* INT 2: D6
* INT 3: D7
* INT 4: D8

## Software
 Der WEMOS öffnet ein WLAN Netz:

SSID: divebot

PW: linus

Nach dem verbinden ruft man einfach im Browser auf:
`http://192.168.4.1/?speed=0&lenk=70`

Hierfür gibt es auch die in Qt geschriebene App.