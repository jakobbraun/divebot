#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlEngine>
#include <QQmlComponent>
#include "botconnector.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<BotConnector>("BotConnector", 1,0, "BotConnector");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
