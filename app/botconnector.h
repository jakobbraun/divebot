#ifndef BOTCONNECTOR_H
#define BOTCONNECTOR_H
#include <QNetworkAccessManager>

class BotConnector : public QObject
{
Q_OBJECT

public:
    BotConnector();

    Q_PROPERTY(float speed READ getSpeed WRITE setSpeed)
    Q_PROPERTY(float course READ getCourse WRITE setCourse)

    float getSpeed() const;
    void setSpeed(float value);

    float getCourse() const;
    void setCourse(float value);

private:
    float speed, course;

    QNetworkAccessManager manager;

    void sendData();
};

#endif // BOTCONNECTOR_H
