#include "botconnector.h"

#include <QNetworkRequest>
#include <QUrl>
#include <QDebug>


BotConnector::BotConnector() :
    QObject(),
    speed(0),
    course(0.5),
    manager(this)
{
//    connect(manager, SIGNAL(finished(QNetworkReply*)),
//            this, SLOT(replyFinished(QNetworkReply*)));
}

float BotConnector::getSpeed() const
{
    return speed;
}

void BotConnector::setSpeed(float value)
{
    speed = value;
    sendData();
}

float BotConnector::getCourse() const
{
    return course;
}

void BotConnector::setCourse(float value)
{
    course = value;
    sendData();
}

void BotConnector::sendData()
{
    QString url = "http://192.168.4.1/?speed="
            + QString::number(int(speed*100.0f))
            + "&lenk=" + QString::number(int(course * 180.0f));
    qDebug() << url;
    manager.get(QNetworkRequest(QUrl(url)));
}
