import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import BotConnector 1.0
import QtSensors 5.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    RowLayout{
        anchors.fill: parent;
        Dial{
            id: speedSlider
            from: 0
            to: 1
            stepSize: 0.01
            onValueChanged:{
                bot.speed = value;
            }
            enabled: false
            Layout.fillWidth: true;
        }
        Dial{
            id: courseSlider
            from: 1
            to: 0
            stepSize: 0.01
            onValueChanged:{
                bot.course = value;
            }
            enabled: false
            Layout.fillWidth: true;
        }
    }

    BotConnector{
        id:bot
    }

    Accelerometer{
        id: acc
        onReadingChanged: {
            console.log("x:" + acc.reading.x + "    y: "+ acc.reading.y + "    z: "+ acc.reading.z);
            var sp = Math.min(Math.abs(acc.reading.x),8.0) / 8.0;
            if(sp < 0.1) sp = 0;
            speedSlider.value = sp;

            courseSlider.value = 0.5 - Math.min(acc.reading.y,8.0)/16.0;

        }
        dataRate: 10
        active: true
    }


}
